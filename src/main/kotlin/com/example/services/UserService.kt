package com.example.services

import com.example.models.dto.post
import com.example.models.schema.User
import com.example.models.schema.UserInfo
import com.example.models.schema.allPost
import com.example.repo.DatabaseConnection
import com.example.utils.FailureExceptions
import com.example.utils.InputValidator
import io.ktor.http.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class UserService {
    private val inputValidator: InputValidator = InputValidator()
    suspend fun getAllPosts(): List<post> {
        return DatabaseConnection().getAllUser()
    }

    suspend fun getPostById(userId: String?): List<post> {
        inputValidator.validateUserId(userId)
        val doc = DatabaseConnection().checkUserInPosts(userId!!)
        if (doc == null) {
            throw FailureExceptions(status = HttpStatusCode.BadRequest, msg = "no user found with userId $userId")
        }
        val ref = DatabaseConnection().getUserById(userId)
        if (ref == null) {
            throw FailureExceptions(status = HttpStatusCode.InternalServerError, msg = "server error")
        }
        return ref

    }

    suspend fun addPosts(user: User, userId: String?): List<post> {
        inputValidator.validateUser(user)
        inputValidator.validateUserId(userId)
        val ref = DatabaseConnection().addUser(user = user, userId = userId)
        if (ref == null) {
            throw FailureExceptions(status = HttpStatusCode.InternalServerError, msg = "server error")
        }
        val doc = DatabaseConnection().checkUserInPosts(userId = userId!!)
        if (doc == null) {
            throw FailureExceptions(status = HttpStatusCode.BadRequest, msg = "no user found with userId $userId")
        }
        return getPostById(userId = userId)
    }

    suspend fun deleteUser(userId: String?): List<post> {
        inputValidator.validateUserId(userId)

        val doc = DatabaseConnection().checkUserInPosts(userId!!)
        if (doc == null) {
            throw FailureExceptions(status = HttpStatusCode.BadRequest, msg = "no user found with userId $userId")
        }

        runBlocking {
            val ref = DatabaseConnection().deleteUserById(userId!!)
            delay(50)
            if (ref == null) {
                throw FailureExceptions(status = HttpStatusCode.InternalServerError, msg = "server error")
            }
        }

        return getAllPosts()
    }
    suspend fun updateUserInfo(userId:String?,userInfo: UserInfo): List<post> {
        inputValidator.validateUserId(userId)
        val doc = DatabaseConnection().checkUserInPosts(userId!!)
        if (doc == null) {
            throw FailureExceptions(status = HttpStatusCode.BadRequest, msg = "no user found with userId $userId")
        }
        val ref = DatabaseConnection().updateUserInfo(userInfo = userInfo,userId = userId)
        println(ref)
        if(ref == null){
            throw FailureExceptions(status = HttpStatusCode.InternalServerError, msg = "server error")
        }
        return getPostById(userId = userId)
    }


}