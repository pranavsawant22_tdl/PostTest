package com.example

import com.example.config.MongoConfig
import com.example.config.Configuration
import io.ktor.server.application.*
import com.example.plugins.*
import com.typesafe.config.ConfigFactory
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.config.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.contentnegotiation.*

class HoconConfiguration{
    var config: HoconApplicationConfig? = null;

    constructor(){
        config = HoconApplicationConfig(ConfigFactory.load())
    }
}fun main() {
    Configuration.initConfig(HoconConfiguration())
    MongoConfig.getDatabase()
    embeddedServer(Netty, port = Configuration.env.port, host = "127.0.0.1") {

        install(ContentNegotiation){
            json()
        }

        configureRouting()


    }.start(wait = true)
}
