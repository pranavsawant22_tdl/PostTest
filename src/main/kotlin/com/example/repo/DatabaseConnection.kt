package com.example.repo


import com.example.config.MongoConfig
import com.example.models.dto.post
import com.example.models.schema.User
import com.example.models.schema.UserInfo
import com.example.models.schema.allPost
import com.mongodb.client.result.UpdateResult
import org.litote.kmongo.coroutine.CoroutineCollection
import org.litote.kmongo.eq
import org.litote.kmongo.formatJson
import java.time.LocalDateTime
import java.util.*

class DatabaseConnection {
    private val userCollection: CoroutineCollection<post> = MongoConfig.getDatabase().getCollection()


    suspend fun checkUserInPosts(userId : String) : post? {
        return userCollection.findOne(post::userId eq userId)
    }


    suspend fun addUser(user: User, userId: String?): User {
        if (userCollection.findOne(post::userId eq userId!!) != null) {
            userCollection.updateOneById(
                userId!!,
                update = "{\$push:{  post :{userId:'${
                    UUID.randomUUID().toString()
                }', curPosts:'${user.posts}' , description:'${user.description}' , author: '${user.name}' ,timeStamp :'${
                    LocalDateTime.now().toString()
                }'}}}".formatJson()
            )
            return user
        }
        val c: MutableList<allPost> = mutableListOf()
        user.userId = UUID.randomUUID().toString()
        val ex = allPost(
            userId = user.userId,
            curPosts = user.posts,
            description = user.description,
            author = user.name,
            timeStamp = LocalDateTime.now().toString()
        )
        c.add(ex)
        val r =
            post(userId = userId.toString(), name = user.name, email = user.email, password = user.password, post = c)
        userCollection.insertOne(r)

        return user
    }

    suspend fun getAllUser(): List<post> {
        return userCollection.find().toList()
    }

    suspend fun getUserById(userId: String): List<post> {
        return userCollection.find(post::userId eq userId).toList()
    }

    suspend fun deleteUserById(userId: String): Boolean {
        return userCollection.deleteOne(post::userId eq userId).wasAcknowledged()
    }
    suspend fun updateUserInfo(userInfo : UserInfo,userId:String?): UpdateResult? {
        if(userCollection.findOne(post :: userId eq userId) != null){
            return userCollection.updateOneById(userId!! , update = "{\$set:{userId:'${userId}' , name:'${userInfo.name}' , email : '${userInfo.email}' , password:'${userInfo.password}'}}".formatJson())

        }
        return null
    }



}