package com.example.plugins


import io.ktor.server.routing.*
import io.ktor.server.application.*
import com.example.routes.appRouting

fun Application.configureRouting() {
    routing {
        appRouting()
    }
}
