package com.example.models.dto

import com.example.models.schema.allPost
import org.bson.codecs.pojo.annotations.BsonId
import org.bson.types.ObjectId

@kotlinx.serialization.Serializable
data class post(
    @BsonId
    val userId:String = ObjectId().toString(),
    val name:String?,
    val email : String?,
    val password : String?,
    val post:MutableList<allPost>
)

data class ConfigurationDTO(
    val databaseUrl: String,
    val databaseName: String,
    val port: Int,
)