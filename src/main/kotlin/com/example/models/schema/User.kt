package com.example.models.schema


import org.bson.codecs.pojo.annotations.BsonId
import org.bson.types.ObjectId
import java.util.UUID

@kotlinx.serialization.Serializable
data class User(

    var userId:String?=UUID.randomUUID().toString() ,
    val name : String?,
    val email : String?,
    val password : String?,
    val posts : String?,
    val description:String?
)
