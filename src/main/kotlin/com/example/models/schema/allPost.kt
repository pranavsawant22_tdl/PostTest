package com.example.models.schema


import java.time.LocalDateTime
import java.util.*


@kotlinx.serialization.Serializable
data class allPost(
    var userId:String?= UUID.randomUUID().toString(),
    var curPosts:String?,
    var description:String?,
    var author:String?,
    var timeStamp: String? = LocalDateTime.now().toString()
    )
