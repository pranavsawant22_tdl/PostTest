package com.example.models.schema

@kotlinx.serialization.Serializable
data class UserInfo(
    val name:String?,
    val email:String?,
    val password:String?
)
