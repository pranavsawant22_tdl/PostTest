package com.example.utils

import com.example.models.dto.post
import com.example.models.schema.User
import io.ktor.http.*

class InputValidator {
    suspend fun validateUserId(userId : String?){
        if(userId == null){
            throw FailureExceptions(status = HttpStatusCode.BadRequest,msg = "user id is missing")
        }

        if(userId == ""){
            throw FailureExceptions(status = HttpStatusCode.BadRequest,msg = "user id is empty")
        }
    }


    suspend fun validateUser(userAddress : User){
        if(userAddress.name == ""){
            throw FailureExceptions(status = HttpStatusCode.BadRequest,msg = "name is missing")
        }
        if(userAddress.email == ""){
            throw FailureExceptions(status = HttpStatusCode.BadRequest,msg = "email info is missing")
        }
        if(userAddress.password == ""){
            throw FailureExceptions(status = HttpStatusCode.BadRequest,msg = "password is missing")
        }
        if(userAddress.posts == ""){
            throw FailureExceptions(status = HttpStatusCode.BadRequest,msg = "post cannot be empty")
        }
        if(userAddress.description == ""){
            throw FailureExceptions(status = HttpStatusCode.BadRequest,msg = "description is missing")
        }
        if(userAddress.userId == ""){
            throw FailureExceptions(status = HttpStatusCode.BadRequest,msg = "userId is missing")
        }
    }
}