package com.example.utils

import com.example.models.dto.post
import com.example.models.schema.User
import kotlinx.serialization.Serializable

@Serializable
data class ResponseFailure(
    val success : Boolean,
    val message : String?
)

@Serializable
data class ResponseSuccess(
    val success : Boolean,
    val Posts : List<post>
)