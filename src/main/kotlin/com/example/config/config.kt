package com.example.config

import com.example.HoconConfiguration
import com.example.models.dto.ConfigurationDTO


object Configuration {
     lateinit var env: ConfigurationDTO

    fun initConfig(environment: HoconConfiguration) {
        env = ConfigurationDTO(
            databaseUrl = environment.config?.property("ktor.envConfig.db.database_url")?.getString() ?: "",
            databaseName = environment.config?.property("ktor.envConfig.db.database_name")?.getString() ?: "",
            port = environment.config?.property("ktor.deployment.port")?.getString()?.toInt() ?: 8080,
        )
    }
}
