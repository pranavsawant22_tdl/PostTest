package com.example.routes

import com.example.models.schema.User
import com.example.models.schema.UserInfo
import com.example.models.schema.allPost
import com.example.repo.DatabaseConnection
import com.example.services.UserService
import com.example.utils.FailureExceptions
import com.example.utils.ResponseFailure
import com.example.utils.ResponseSuccess
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.appRouting() {

    route("/user") {
        get("/") {
            val userId: String? = call.request.headers["userId"]
            try {
                call.respond(
                    status = HttpStatusCode.OK,
                    message = ResponseSuccess(
                        success = true,
                        Posts = UserService().getPostById(userId = userId)
                    )
                )
            } catch (e: FailureExceptions) {
                call.respond(
                    status = e.status,
                    message = ResponseFailure(
                        success = false,
                        message = e.message
                    ),
                )
            }
//            call.respond(db.getUserById(userId!!))
        }
        get("/all") {
            try {
                call.respond(
                    status = HttpStatusCode.OK,
                    message = ResponseSuccess(
                        success = true,
                        Posts = UserService().getAllPosts()
                    )
                )
            } catch (e: FailureExceptions) {
                call.respond(
                    status = e.status,
                    message = ResponseFailure(
                        success = false,
                        message = e.message
                    ),
                )
            }
//            call.respond(db.getAllUser())
        }
        post("/") {
            val user: User = call.receive()
            val userId = call.request.headers["userId"]
            try {
                call.respond(
                    status = HttpStatusCode.OK,
                    message = ResponseSuccess(
                        success = true,
                        Posts = UserService().addPosts(user = user, userId = userId)
                    )
                )
            } catch (e: FailureExceptions) {
                call.respond(
                    status = e.status,
                    message = ResponseFailure(
                        success = false,
                        message = e.message
                    ),
                )
            }
//            call.respond(db.addUser(user = user, userId!!))

        }

        delete("/{userId}") {
            val userId: String? = call.parameters["userId"]
            try {
                call.respond(
                    status = HttpStatusCode.OK,
                    message = ResponseSuccess(
                        success = true,
                        Posts = UserService().deleteUser(userId = userId)
                    )
                )
            } catch (e: FailureExceptions) {
                call.respond(
                    status = e.status,
                    message = ResponseFailure(
                        success = false,
                        message = e.message
                    ),
                )
            }
//            call.respond(db.deleteUserById(userId!!))
        }
        put("/update"){
            val userInfo:UserInfo = call.receive()
            val userId = call.request.headers["userId"]
            try {
                call.respond(
                    status = HttpStatusCode.OK,
                    message = ResponseSuccess(
                        success = true,
                        Posts = UserService().updateUserInfo(userId = userId, userInfo = userInfo)
                    )
                )
            } catch (e: FailureExceptions) {
                call.respond(
                    status = e.status,
                    message = ResponseFailure(
                        success = false,
                        message = e.message
                    ),
                )
            }
        }
//        put("/updatePost/{postId}"){
//            val postInfo:allPost = call.receive()
//            val userId = call.request.headers["userId"]
//            val postId = call.parameters["postId"]
//            try {
//                call.respond(
//                    status = HttpStatusCode.OK,
//                    message = ResponseSuccess(
//                        success = true,
//                        Posts = UserService().updatePost(userId = userId, postInfo = postInfo,postId = postId)
//                    )
//                )
//            } catch (e: FailureExceptions) {
//                call.respond(
//                    status = e.status,
//                    message = ResponseFailure(
//                        success = false,
//                        message = e.message
//                    ),
//                )
//            }
//        }
    }
}